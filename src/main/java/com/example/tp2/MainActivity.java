package com.example.tp2;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Ma listeView
    ListView listView;
    //Ma bdd
    WineDbHelper dns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list);

        //afficher la list view

        dns = new WineDbHelper(getApplicationContext());

        //si la bdd est vide // on appelle populate -->>> pour remplir la bdd
        if (dns.fetchAllWines().getCount() < 1) {
            dns.populate();
        }
        rafri_affich();



        //click sur un element(item) de la liste -> change d'acytivity (wine)
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

             //creation d'un courseur qui pointe vers l'item selectionné grace a sa position
             final Cursor item = (Cursor) parent.getItemAtPosition(position);

             // Création de l'intent
            Intent intent = new Intent(MainActivity.this, activity_wine.class);

             // Ajout dans l'intent de l'objet wine
             intent.putExtra("wine", dns.cursorToWine(item));

             // Envoyer l'intent qui l'objet
             startActivity(intent);

         }
     });


        //Buttom de rajout de vins
        FloatingActionButton buttom =findViewById(R.id.btm);
        buttom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent1 = new Intent(MainActivity.this, activity_wine.class);
                startActivity(intent1);

            }
        });

        //(Dans le cas de supprimer) : Elle spicifie que ma liste a des menus a chaque fois que je click sur un item
        registerForContextMenu(listView);

    }



//On utilise une fonction de refrich qui fait une selection entiere sur la bdd
    //Donc elle recupere la table et elle adapte a la listeView

    public void rafri_affich(){
        //recuperer tt les lignes de la bdd
        Cursor resultat = dns.fetchAllWines();
        //curseur reviens au debut
        resultat.moveToFirst();

        //adapter la selection du curseur a la listView
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,
                resultat, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_LOC},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        //Afichage de notre listView
        listView.setAdapter(adapter);
    }



    //creation de menu attaché a la listeView
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Delete");
    }

    //si on selectionne "delete" c cette fonction qui s'execute ---> supprime l'item
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Delete") {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor it = (Cursor) listView.getItemAtPosition(info.position);

            dns.deleteWine(it);
            rafri_affich();

        }
        return true;
    }

    /*
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            final ArrayList<Object> itemlist = new ArrayList<>();
            ArrayAdapter<Object> adapter1 = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_multiple_choice, itemlist);


            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                SparseBooleanArray positioncheker = listView.getCheckedItemPositions();
                int count = listView.getCount();

                for(int item = count-1; item>= 0; item--){

                    if(positioncheker.get(item)){
                        adapter1.remove(itemlist.get(item));
                        Toast.makeText(MainActivity.this,"item supprim",Toast.LENGTH_SHORT).show();
                    }
                }
                positioncheker.clear();
                adapter1.notifyDataSetChanged();

                return false;
            }
        });

*/
}
