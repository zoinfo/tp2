package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class activity_wine extends AppCompatActivity {

    //bdd
    WineDbHelper base = new WineDbHelper(activity_wine.this);
    //objet courant vide
    final   Wine wine1 = new Wine();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);


        final EditText Nom = (findViewById(R.id.wineName));
        final EditText region = (findViewById(R.id.editWineRegion));
        final EditText loc = (findViewById(R.id.editLoc));
        final EditText climat = (findViewById(R.id.editClimate));
        final EditText area = (findViewById(R.id.editPlantedArea));

        final Button button = (Button) findViewById(R.id.button);

        //On recupere l'intent envoyé de la 1ere activity MainActivity
        Intent intent=getIntent();

        //verifie si l'intent recu a du contenu
        if(intent.hasExtra("wine")) {

            //recuperer l'objet qui est dans l'intent et on le met dans wine.
            final Wine wine = (Wine) intent.getParcelableExtra("wine");

            //Afficher les infos du vin dans la 2éme activity
            Nom.setText(wine.getTitle());
            region.setText(wine.getRegion());
            loc.setText(wine.getLocalization());
            climat.setText(wine.getClimate());
            area.setText(wine.getPlantedArea());

            //quand on click sur un element de la liste : on fait un changement -> sauvegarder

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //l'objet qu'on a recuperer de l'autre activity
                    //modifier dans la bdd
                    //settitle : changer le nom dans la bdd par :
                    //Nom.getText().toString() : le nom ecrit par l'utilisateur

                    wine.setTitle(Nom.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setLocalization(loc.getText().toString());
                    wine.setClimate(climat.getText().toString());
                    wine.setPlantedArea(area.getText().toString());

                    //appel a a fonction mise a j
                    base.updateWine(wine);


                    //cnl.sauvgarde(n);
                    Toast.makeText(activity_wine.this, "Sauvegarde établie avec succés", Toast.LENGTH_LONG).show();
                }
            });
        }



        else {

            //si je click sur le buttom plus rose
            button.setOnClickListener(new View.OnClickListener() {



                @Override
                public void onClick(View v) {

                    //donner les infos pour le nouveau objet qui est vide
                    //initialiser title((ce qu'on a ecrit grace a getText)) a notre objet wine(vide)
                    wine1.setTitle(Nom.getText().toString());
                    wine1.setRegion(region.getText().toString());
                    wine1.setLocalization(loc.getText().toString());
                    wine1.setClimate(climat.getText().toString());
                    wine1.setPlantedArea(area.getText().toString());


                    //verifier que le champ title est vide ----> ALERTE
                    if(wine1.getTitle().matches("") || wine1.getRegion().matches("") || wine1.getLocalization().matches("")
                        || wine1.getClimate().matches("") || wine1.getPlantedArea().matches(""))
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity_wine.this);
                        builder.setMessage("Un champs de votre Vin n'est pas saisi");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }

                    /*

                    if(wine1.getTitle().equals(Nom.getText().toString())){
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity_wine.this);
                        builder.setMessage("Ajout impossible");
                      //  builder.setMessage("Ajout impossible");
                        builder.setCancelable(true);
                        builder.show();
                        return;

                    }
*/

                    //Ajouter l'element et revenir sur la 1ere activity
                    base.addWine(wine1);
                    Intent intent2 = new Intent(activity_wine.this, MainActivity.class);
                    startActivity(intent2);



                }
            });
        }


    }
}
